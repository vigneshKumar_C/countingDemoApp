//
//  ViewController.swift
//  countingDemoApp
//
//  Created by vignesh kumar c on 15/11/22.
//

import UIKit
import SPConfetti

class ViewController: UIViewController {
    
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var labelCounter: UILabel!
    
    @IBAction func incrementButtonTapped(_ sender: Any) {
        
        labelCounter.layer.bottomAnimation(duration: 0.4)
        counter = counter + 1
        labelCounter.text = "\(counter)"
    }
    
    @IBAction func decrementButtonTapped(_ sender: Any) {
        if counter > 0 {
            labelCounter.layer.topAnimation(duration: 0.4)
            counter = counter - 1
            labelCounter.text = "\(counter)"
        }
    }
    
    @IBAction func sumitBtnTapped(_ sender: Any) {
        SPConfetti.startAnimating(.centerWidthToDown, particles:[.heart, .star,.triangle], duration: 2)
    }
}

extension CALayer {
    
    func bottomAnimation(duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.duration = duration
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromTop
        self.add(animation, forKey: kCATransition)
    }
    
    func topAnimation(duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.duration = duration
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromBottom
        self.add(animation, forKey: kCATransition)
    }
}

